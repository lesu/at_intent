package personal.intent;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private Button button1;
    private Button button2;
    private EditText editTextIn;
    private Intent intent;
    final static String INTENT_EXTRA_NAME_1 = "text";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.initComponents();
    }



    private void initComponents() {
        this.button1 = (Button) this.findViewById(R.id.button_1);
        this.button2 = (Button) this.findViewById(R.id.button_2);
        this.editTextIn = (EditText) this.findViewById(R.id.edit_text_in);

        this.button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = MainActivity.this.editTextIn.getText().toString();
                MainActivity.this.editTextIn.setText("");
                MainActivity.this.intent = new Intent(MainActivity.this.getApplicationContext(),WelcomeActivity.class);
                MainActivity.this.intent.putExtra(MainActivity.INTENT_EXTRA_NAME_1,text);
                MainActivity.this.startActivity(MainActivity.this.intent);
            }
        });


        this.button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = MainActivity.this.editTextIn.getText().toString();
                MainActivity.this.intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com/search?q="+text));
                MainActivity.this.startActivity(MainActivity.this.intent);
            }
        });
    }

}
