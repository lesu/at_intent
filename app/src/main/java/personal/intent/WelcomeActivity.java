package personal.intent;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class WelcomeActivity extends AppCompatActivity {
    private TextView textViewWelcome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        this.initComponents();
    }



    private void initComponents() {
        this.textViewWelcome = (TextView) this.findViewById(R.id.text_view_welcome);
        String text = this.getIntent().getStringExtra(MainActivity.INTENT_EXTRA_NAME_1);
        text = (text==null) ? "" : text;
        this.textViewWelcome.setText(text);
    }


}
